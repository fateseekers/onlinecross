import React from "react"
import renderer from "react-test-renderer"
import { Provider } from "react-redux"
import { persistReducer, persistStore } from "redux-persist"
import { PersistGate } from 'redux-persist/integration/react'
import { createStore } from "redux"
import { composeWithDevTools } from "redux-devtools-extension"
import storage from 'redux-persist/lib/storage'
import { IntlProvider } from 'gatsby-plugin-intl'

import Order from "../src/components/forms/order"

const persistConfig = {
  key: 'root',
  storage,
  blacklist: [
    // 'count',
  ],
};

const initialState = {
  cart: []
}

const rootReducer = (state = initialState, action) => {
  switch (action.type) {
    case "ADDTOCART":
      return Object.assign({}, state, {
        cart: [
          ...state.cart,
          action.payload
        ]
      })
    case "REMOVEGOOD":
      let cart = state.cart
      let index = cart.findIndex(n => n.id === action.payload.id);
      cart.splice(index, 1)
      return Object.assign({}, state, {
        cart: [
          ...cart
        ]
      })
    case "RESETCART":
      return Object.assign({}, state, {
        cart: []
      })
    default:
      return state
  }
}

const persistedReducer = persistReducer(persistConfig, rootReducer);
const store = createStore(persistedReducer, composeWithDevTools());
const persistor = persistStore(store);

describe("Cart Page", () => {
  it("renders correctly", () => {
    const tree = renderer
      .create(
        <IntlProvider locale={"ru"}>
          <Provider store={store}>
            <PersistGate loading={null} persistor={persistor}>
              <Order />
            </PersistGate>
          </Provider>
        </IntlProvider>
      )
      .toJSON()
    expect(tree).toMatchSnapshot()
  })
})