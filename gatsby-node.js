const path = require(`path`)

// pages locale
exports.onCreatePage = ({ page, actions }) => {
  const { createPage, deletePage } = actions
  console.log(page)
  deletePage(page)
  // You can access the variable "locale" in your page queries now
  createPage({
    ...page,
    context: {
      ...page.context,
      locale: page.context.intl.language,
    },
  })
}

// categories and goods
exports.createPages = ({ graphql, actions }) => {
  const { createPage } = actions
  const category = path.resolve(`src/templates/category.js`)
  const goods = path.resolve(`src/templates/good.js`)
  // Query for markdown nodes to use in creating pages.
  // You can query for whatever data you want to create pages for e.g.
  // products, portfolio items, landing pages, etc.
  // Variables can be added as the second function parameter
  return graphql(`
    query MyQuery {
      Api {
        all_categories {
          id
          ru
          en
        }
        goods {
          id
          ru
          en
        }
      }
    }
  `
  ).then(result => {
    if(result.errors) {
      throw result.errors
    }

    // Create categories pages.
    result.data.Api.all_categories.forEach(category => {
      const path = category.en.toLowerCase().replace(/\s/ig, "_")
      const id = category.id

      createPage({
        // Path for this page — required
        path: "/categories/" + path,
        component: require.resolve("./src/templates/category"),
        context: {
          id: id,
        },
      })
    })
    // Create goods pages.
    result.data.Api.goods.forEach(good => {
      const path = good.en.toLowerCase().replace(/\s/ig, "_")
      const id = good.id

      createPage({
        // Path for this page — required
        path: "/goods/" + path,
        component: require.resolve("./src/templates/good"),
        context: {
          id: id,
        },
      })
    })
  })
}