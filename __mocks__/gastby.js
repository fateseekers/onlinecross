const React = require("react")
const gatsby = jest.requireActual("gatsby")
module.exports = {
  ...gatsby,
  graphql: jest.fn(
    `{
       categories {
          id 
          ru
          en
          img
          goods {
            id
            ru
            en
            img
            description
            specification
            colors {
              id
              code
            }
            sizes {
              id
              size
              description
            }
          }
          child_categories {
            id 
            ru
            en
            img
          goods {
            id
            ru
            en
            img
            description
            specification
            colors {
              id
              code
            }
            sizes {
              id
              size
              description
            }
          }
        }
      }
    `),
  Link: jest.fn().mockImplementation(
    // these props are invalid for an `a` tag
    ({
       activeClassName,
       activeStyle,
       getProps,
       innerRef,
       partiallyActive,
       ref,
       replace,
       to,
       ...rest
     }) =>
      React.createElement("a", {
        ...rest,
        href: to,
      })
  ),
  StaticQuery: jest.fn(),
  useStaticQuery: jest.fn(),
}