import React, { useState } from "react"
import { injectIntl, Link, FormattedMessage, useIntl } from "gatsby-plugin-intl"
import { MDBBtn, MDBContainer, MDBModal, MDBModalBody, MDBModalHeader } from "mdbreact"
import { connect } from "react-redux"
import { toast } from 'react-toastify';
import Order from '../../forms/order'

const GoodsCard = ({good, addToCart}) => {

  const [modal, setModal] = useState(false)

  function toggle () {
    setModal(!modal)
  }

  const intl = useIntl()

  function selectColor(e) {
      let colors = document.querySelectorAll(".good__card--color")
      colors.forEach(color => {
        color.classList.remove("active")
      })
      e.target.classList.add('active')
  }

  function addToCartFunction (e){
    addToCart(good)
    toast.success(
      intl.locale === "en"
        ? "Good is added to cart"
        : "Товар добавлен в корзину"
    )
  }

  return (
    <>
      <div className="good__card d-flex flex-wrap">
        <div className="col-12 col-md-6 d-flex flex-column">
          <img className="good__card--image" src={good.img} alt=""/>
        </div>
        <div className="col-12 col-md-6 d-flex flex-column">
          <p>
            <FormattedMessage id="colors" />
          </p>
          <div className="good__card--colors">
            {good.colors.map((color) => {
              return (
                <span key={color.id} className="good__card--color" style={{backgroundColor: color.code}} onClick={(e) => selectColor(e)}/>
              )
            })}
          </div>
          <hr/>
          <p>
            <FormattedMessage id="sizes.sizes" />
          </p>
          <div className="good__card--sizes">
            {good.sizes && good.sizes.length !== 0
              ? good.sizes.map((size) => {
                return (
                  <table key={size.id} className="table">
                    <tbody>
                      <tr>
                        <td>{size.size}</td>
                        <td>{size.description}</td>
                      </tr>
                    </tbody>
                  </table>
                )
              })
              : <p>
                <FormattedMessage id="sizes.notFound" />
              </p>
            }
          </div>
          <div className="good__card--buttons d-flex flex-column flex-md-row">
            <MDBBtn className="btn-sm good__card--button" color="black" onClick={(e) => addToCartFunction()}>
              <FormattedMessage id="buy.buy" />
            </MDBBtn>
            <MDBBtn className="btn-sm good__card--button" color="black" onClick={() => toggle()}>
              <FormattedMessage id="buy.one"/>
            </MDBBtn>
          </div>
        </div>
      </div>
      <MDBContainer>
        <MDBModal isOpen={modal} toggle={() => toggle()}>
          <MDBModalBody>
            <Order toggleModal={() => toggle()}/>
          </MDBModalBody>
        </MDBModal>
      </MDBContainer>
    </>
  )
}

const mapDispatchToProps = dispatch => {
  return {
    addToCart: (data) => dispatch({ type: 'ADDTOCART', payload: data }),
  }
}

export default connect(null, mapDispatchToProps)(GoodsCard)