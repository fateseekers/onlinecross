import React from 'react'
import bg from "../../../assets/img/bg.jpg"
import { injectIntl, Link, FormattedMessage, useIntl } from "gatsby-plugin-intl"
import Card from "../card"

const Categories = ({category}) => {
  const intl = useIntl()
  return (
    <div className="content__card">
      <div className="content__card--text">
        <h3 className="content__card--heading">{intl.locale === "ru" ? category.ru : category.en}</h3>
        <Link to={`/categories/${category.en.toLowerCase().replace(/\s/ig, "_")}`} className="content__card--button">
          <FormattedMessage id={"buttons.more"}/>
        </Link>
      </div>
      <div className="content__card--image_block">
        <img className="content__card--image" src={category.img} alt={intl.locale === "ru" ? category.ru : category.en}/>
      </div>
    </div>
  )
}

export default Categories