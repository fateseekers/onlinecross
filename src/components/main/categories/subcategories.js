import React from 'react'
import { Link } from "gatsby-plugin-intl"
import { useIntl } from 'gatsby-plugin-intl'

const SubCategories = ({category}) => {
  const intl = useIntl()

  return (
    category.child_categories.map((category) => {
      return <ul key={category.id} className="nav__list dropdown__list drop_close">
        <li className="nav__item">
          <Link className="nav__link" to={`/categories/${category.en.toLowerCase().replace(/\s/ig, "_")}`} as={`/categories/${category.en}`}>
            {intl.locale === "ru" ? category.ru : category.en}
          </Link>
        </li>
      </ul>
    })
  )
}

export default SubCategories