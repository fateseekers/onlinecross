import React from 'react'
import { Link } from "gatsby-plugin-intl"
import { useIntl } from 'gatsby-plugin-intl'
import SubCategories from "./subcategories"
import { MDBIcon } from "mdbreact"

const CategoriesList = ({categories}) => {
  const intl = useIntl()

  function dropDown (e) {
    e.preventDefault();
    let el = e.target,
        nav_el = el.closest(".nav__item"),
        dropdown = nav_el.querySelector(".dropdown__list");

    if(dropdown.classList.contains("drop_close")){
      dropdown.classList.remove("drop_close")
      el.style.transform = "rotate(180deg)"
      dropdown.classList.add("drop_open")

    } else {
      dropdown.classList.remove("drop_open")
      el.style.transform = "rotate(0deg)"
      dropdown.classList.add("drop_close")
    }
  }

  return (
    <ul className="nav__list pt-2">
      {categories && categories.length !== 0
        ? categories.map((category) => {
          return <li key={category.id} className="nav__item d-flex flex-column">
            <Link className="nav__link d-flex align-items-center justify-content-between" to={`/categories/${category.en.toLowerCase().replace(/\s/ig, "_")}`}>
              {intl.locale === "ru" ? category.ru : category.en}
              {category.child_categories !== null
                ? <MDBIcon className="dropdown__icon" size="2x" icon="angle-down" onClick={(e) => dropDown(e)}/>
                : null
              }
            </Link>
            {category.child_categories !== null
              ? <SubCategories category={category}/>
              : null
            }
          </li>
        })
        : null
      }
    </ul>
  )
}

export default CategoriesList