import React from 'react'
import bg from "../../assets/img/bg.jpg"
import { injectIntl, Link, FormattedMessage, useIntl } from "gatsby-plugin-intl"
import { MDBIcon } from "mdbreact"
import photo from '../../assets/img/bg.jpg'

const Card = ({link, title, img}) => {
  console.log(img)
  return (
    <Link className="categories__card--link" to={link.toLowerCase().replace(/\s/ig, "_")}>
      <div className="categories__card z-depth-1">
        <img src={img} alt="" className="categories__card--image" />
        <div className="categories__card--text">
          <h3 className="categories__card--title">
            {title}
            <MDBIcon className="ml-2" icon="arrow-right"/>
          </h3>
        </div>
      </div>
    </Link>
  )
}

export default Card