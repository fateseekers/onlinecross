import React from "react"
import { IntlContextConsumer, changeLocale } from "gatsby-plugin-intl"

const languageName = {
  en: "RU",
  ru: "EN",
}

const Language = () => (
  <div>
    <IntlContextConsumer>
      {({ languages, language: currentLocale }) =>
        languages.map(language => (
          <a
            className="header__language"
            key={language}
            onClick={() => changeLocale(language)}
            style={{
              display: currentLocale === language ? 'none' : 'block',
              fontWeight: 'bold',
              cursor: `pointer`,
            }}
          >
            {languageName[language]}
          </a>
        ))
      }
    </IntlContextConsumer>
  </div>
)

export default Language