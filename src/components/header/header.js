import React, { Component } from "react"
import { injectIntl, Link, FormattedMessage } from "gatsby-plugin-intl"
import Language from "./language"
import { MDBIcon } from "mdbreact"
import Navigation from "./nav/navigation"
import logo from '../../assets/svg/logo.svg'
import Cart from "../cart"

class Header extends Component{
  constructor(props){
    super(props)

    this.state = {
      isOpen: false,
    }

    this.toggleShow = this.toggleShow.bind(this)
    this.headerMenu = React.createRef();
  }

  componentDidMount() {
    let el = this.headerMenu.current

    document.addEventListener('wheel', function(e){
      if(window.pageYOffset > 50){
        el.style.background = "black"
      } else {
        el.style.background = "transparent"
      }
    })
  }

  toggleShow(){
    this.setState({isOpen: !this.state.isOpen});
  }

  render(){
    return (
      <header className="header" ref={this.headerMenu}>
        <div className="container d-flex justify-content-center">
          <div className="d-flex align-items-center justify-content-between">
            <MDBIcon className="header__bars"
                     icon={!this.state.isOpen ? `bars` : `times`}
                     onClick={() => {
                       this.toggleShow()
                     }}
                     style={{color: this.state.isOpen ? "black" : "white"}}
            />
            <div className="header__logo--block">
              <img className="header__logo--image" src={logo} alt="" />
                {/*<FormattedMessage className="header__title" id={"headerTitle"}/>*/}
            </div>
            <div className="d-flex align-items-center">
              <Language/>
              <Cart />
            </div>
            <Navigation show={this.state.isOpen} categories={this.props.categories}/>
          </div>
        </div>
      </header>
    )
  }
}

export default injectIntl(Header)
