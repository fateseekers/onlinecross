import React, { useState } from "react"
import { injectIntl, Link, FormattedMessage, changeLocale, useIntl } from "gatsby-plugin-intl"
import CategoriesList from "../../main/categories/list"
import { MDBInput } from "mdbreact"
import { navigate } from "gatsby"

const Navigation = ({show, categories}) => {
  const intl = useIntl()

  function onChange(e){
    setStr(e.target.value)
  }

  function onSubmit(e){
    e.preventDefault();

    if(window.location.pathname === ("/en/search" || "/ru/search")){
      window.location.search = `?str=${str}`
    } else {
      changeLocale(intl.locale, `/search?str=${str}`)
    }
  }

  const [str, setStr] = useState("");

  return(
      <nav className={`nav${show ? " show" : ""} z-depth-2`}>
        <ul className="nav__list">
          <li className="nav__item">
            <Link className="nav__link" to={"/"}>
              <FormattedMessage id={"urls.main"} />
            </Link>
          </li>
          <li className="nav__item">
            <Link className="nav__link" to={"/categories"}>
              <FormattedMessage id={"urls.categories"} />
            </Link>
            <CategoriesList categories={categories}/>
            <form onSubmit={(e) => onSubmit(e)}>
              <MDBInput label={<FormattedMessage id={"search.search"}/>} onChange={(e) => onChange(e)}/>
            </form>
          </li>
        </ul>
      </nav>
  )
}


export default injectIntl(Navigation)