import React from 'react'
import { MDBBtn, MDBIcon, MDBInput } from "mdbreact"
import { injectIntl, Link, FormattedMessage, useIntl } from "gatsby-plugin-intl"
import { toast } from "react-toastify"
import { connect } from "react-redux"

const Order = ({resetCart, toggleModal}) => {
  const intl = useIntl()
  function closeModal(){
    toggleModal()
  }

  return (
    <form className="cart__form" onSubmit={(e) => {e.preventDefault(); toast.success(<FormattedMessage id="order.success" />); resetCart(); closeModal()}}>
      <p className="h5 text-center mb-4">
        <FormattedMessage id="order.order" />
      </p>
      <div className="grey-text">
        <MDBInput label={<FormattedMessage id="forms.name" />} icon="user" group type="text" validate error="wrong"
                  success="right" required/>
        <MDBInput label={<FormattedMessage id="forms.email" />} icon="envelope" group type="email" validate error="wrong"
                  success="right" required/>
        <MDBInput label={<FormattedMessage id="forms.phone" />} icon="phone" group type="text" validate error="wrong"
                  success="right" required/>
      </div>
      <div className="text-center">
        <MDBBtn outline color="dark" type="submit">
          <FormattedMessage id="forms.send" />
          <MDBIcon far icon="paper-plane" className="ml-1" />
        </MDBBtn>
      </div>
    </form>
  )
}

const mapDispatchToProps = dispatch => {
  return {
    resetCart: () => dispatch({ type: 'RESETCART'}),
  }
}

export default connect(null, mapDispatchToProps)(Order)