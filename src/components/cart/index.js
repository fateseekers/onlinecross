import React from 'react'
import { MDBIcon } from "mdbreact"
import { connect } from "react-redux"
import { Link } from "gatsby-plugin-intl"

const Cart = ({cart}) => {
  return (
    <div className="d-flex justify-content-center align-items-center header__cart">
      <Link to="/cart">
        <MDBIcon className="header__cart--image" icon="shopping-cart"/>
        <span className="header__cart--count">
          {cart.length}
        </span>
      </Link>
    </div>
  )
}

const mapStateToProps = state => {
  return {
    cart: state.cart
  }
}


export default connect(mapStateToProps, null)(Cart)