import React from "react"
import Header from "./header/header"
import '@fortawesome/fontawesome-free/css/all.min.css';
import 'bootstrap-css-only/css/bootstrap.min.css';
import 'mdbreact/dist/css/mdb.css';
import "./layout.scss"
import 'react-toastify/dist/ReactToastify.css';

import { graphql, useStaticQuery } from "gatsby"
import { ToastContainer } from "react-toastify"

const Layout = ({ children }) => {
  const data = useStaticQuery(graphql`
    query Layout {
      Api {
        categories {
          id
          ru
          en
          child_categories {
            id
            ru
            en
          }
        }
      }
    }
  `)
  return (
    <>
      <Header categories={data.Api.categories}/>
      <main>
        {children}
      </main>
      <footer></footer>
      <ToastContainer />
    </>
  )
}

export default Layout