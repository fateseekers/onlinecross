import React from 'react'
import SEO from "../components/seo"
import Card from "../components/main/card"
import Layout from "../components/layout"
import { injectIntl, Link, FormattedMessage, useIntl } from "gatsby-plugin-intl"
import { graphql, StaticQuery } from "gatsby"

const SearchPage = ({data, location}) => {
  const intl = useIntl()

  let str = location.search.split('str=')[1]
  let filtered = str !== undefined ? data.Api.goods.filter((good) => good.ru.toLowerCase().includes(str.toLowerCase()) || good.en.toLowerCase().includes(str.toLowerCase())) : []


  return (
    <Layout>
      <SEO title="ShoesEvo" keywords="" description=""/>
      <section className="page__section section__bg">
        <div className="container align-items-center">
          <h2 className="page__section--heading">
            <FormattedMessage id={"search.results"}/>
          </h2>
        </div>
      </section>
      <section className="content page__section categories">
        <div className="container">
          <div className="row">
            <div className="col-12 d-flex flex-wrap justify-content-center">
              {filtered && filtered.length !== 0
                ? filtered.map((good) => {
                  return (
                    <Card key={good.id} title={intl.locale === "en" ? good.en : good.ru} link={`/goods/${good.en.toLowerCase()}`} img={good.img}/>
                  )
                })
                : <p>
                  <FormattedMessage id="goods.notfound" />
                </p>
              }
            </div>
          </div>
        </div>
      </section>
    </Layout>
  )
}

export default SearchPage

export const query = graphql`
  query Search {
    Api {
      goods {
        id
        ru
        en
        img
      }
    }
  }
`