import React from 'react'
import SEO from "../components/seo"
import Layout from "../components/layout"
import { injectIntl, Link, FormattedMessage, useIntl } from "gatsby-plugin-intl"
import { connect } from "react-redux"
import Order from "../components/forms/order"
import bg from '../assets/img/background.jpg'
import { MDBIcon } from "mdbreact"

const CartPage = ({cart, removeGood}) => {
  const intl = useIntl()

  return (
    <Layout>
      <SEO title={intl.locale === "en" ? "Cart" : "Корзина"} keywords="123" description="123"/>
      <section className="page__section section__bg">
        <div className="container align-items-center">
          <h1 className="page__section--heading">
            <FormattedMessage id="cart.cart" />
          </h1>
        </div>
      </section>
      <section className="page__section">
        <div className="container">
          {cart && cart.length !== 0
            ? <>
            {cart.map((good) => {
              return <div key={good.id} className="row d-flex flex-column flex-md-row align-items-center">
                <div className="col-12 col-md-6 d-flex justify-content-center">
                  <img className="cart__image" src={good.img} alt=""/>
                </div>
                <div className="col-12 col-md-5 d-flex justify-content-center">
                  <h5 className="mb-0">{intl.locale === "en" ? good.en : good.ru}</h5>
                </div>
                <div className="col-12 col-md-1 d-flex justify-content-center">
                  <MDBIcon icon="times" onClick={() => removeGood(good.id)}/>
                </div>
              </div>
            })}
              <hr/>
              <div className="d-flex cart__order z-depth-1">
                <img className="cart__bg" src={bg}/>
                <Order />
              </div>
            </>
          : <h5 className="text-center">
              <FormattedMessage id="cart.empty" />
            </h5>
          }
        </div>
      </section>
    </Layout>
  )
}

const mapStateToProps = state => {
  return {
    cart: state.cart
  }
}

const mapDispatchToProps = dispatch => {
  return {
    removeGood: (data) => dispatch({ type: 'REMOVEGOOD', payload: data }),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(CartPage)