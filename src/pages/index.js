import React, { Component } from "react"
import Layout from "../components/layout"
import SEO from "../components/seo"
import { injectIntl, Link, FormattedMessage, useIntl } from "gatsby-plugin-intl"

import Categories from "../components/main/categories"
import { graphql } from "gatsby"


class IndexPage extends Component {

  render () {
    return (
      <Layout>
        <SEO title="Homer" keywords="123" description="123"/>
        <section className="page__section section__bg">
          <div className="container align-items-center">
            <h1 className="page__section--heading">
              <FormattedMessage id={"mainSection.heading"}/>
            </h1>
            <Link to="/categories" className="section__bg__button" name="newCollectionButton">
              <FormattedMessage id={"mainSection.button"}/>
            </Link>
          </div>
        </section>
        <section className="content page__section">
          <div className="container">
            <div className="row">
              <div className="col-12">
                <h2 className="content__heading">
                  <FormattedMessage id={"contentSection.heading"}/>
                </h2>
              </div>
            </div>
            <div className="row">
              <div className="col-12 p-0">
                {this.props.data.Api.categories.map((category, index) => {
                  return <Categories key={category.id} key={category.id} category={category} />
                })}
                <div className="mx-auto mt-5 d-flex justify-content-center">
                  <Link className="content__card--button" to="/categories">
                    <FormattedMessage id={"buttons.see"}/>
                  </Link>
                </div>
              </div>
            </div>
          </div>
        </section>
      </Layout>
    )
  }
}

export default injectIntl(IndexPage)

export const PageQuery = graphql`
  query IndexPage {
    Api {
      categories {
        id
        ru
        en
        img
        child_categories {
          id
          ru
          en
        }
      }
    }
  }
`
