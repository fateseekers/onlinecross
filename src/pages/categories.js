import React, { useState } from "react"
import Layout from "../components/layout"
import SEO from "../components/seo"
import { graphql } from "gatsby"
import { injectIntl, Link, FormattedMessage, useIntl } from "gatsby-plugin-intl"
import photo from '../assets/img/bg.jpg'
import { MDBIcon } from "mdbreact"
import Card from "../components/main/card"

const Categories = ({data}) => {
  const intl = useIntl()

  return (
    <Layout>
      <SEO title="ShoesEvo" keywords="" description=""/>
      <section className="page__section section__bg">
        <div className="container align-items-center">
          <h2 className="page__section--heading">
            <FormattedMessage id={"contentSection.heading"}/>
          </h2>
        </div>
      </section>
      <section className="content page__section categories">
        <div className="container">
          <div className="row">
            <div className="col-12 d-flex flex-wrap justify-content-center">
              {data.Api.all_categories.map((category) => {
                return <Card key={category.id} title={intl.locale === "en" ? category.en : category.ru} link={`/categories/${category.en}`} img={category.img}/>
              })}
            </div>
          </div>
        </div>
      </section>
    </Layout>
  )
}

export default Categories

export const PageQuery = graphql`
  query Categories {
    Api {
      all_categories {
        id
        ru
        en
        img
      }
    }
  }
`