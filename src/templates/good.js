import React from 'react'
import { graphql } from "gatsby"
import SEO from "../components/seo"
import Layout from "../components/layout"
import { injectIntl, Link, FormattedMessage, useIntl } from "gatsby-plugin-intl"
import GoodsCard from "../components/main/goods/goodsCard"


const GoodsTemplate = ({data}) => {
  const intl = useIntl()
  return (
    <Layout>
      <SEO title={intl.locale === "en" ? data.Api.good.en : data.Api.good.ru} keywords="123" description="123"/>
      <section className="page__section section__bg">
        <div className="container align-items-center">
          <h1 className="page__section--heading">
            {intl.locale === "en" ? data.Api.good.en : data.Api.good.ru}
          </h1>
        </div>
      </section>
      <section className="page__section">
        <div className="container">
          <div className="row">
            <div className="col-12 d-flex flex-wrap justify-content-center">
              {data.Api.good !== null
                ? <GoodsCard good={data.Api.good}/>
                : <h2 className="text-center mx-auto">
                  <FormattedMessage id={"goods.notfound"} />
                </h2>
              }
            </div>
          </div>
        </div>
      </section>
    </Layout>
  )
}

export default GoodsTemplate

export const query = graphql`
  query Goods ($id: ID!) {
    Api {
      good (id: $id) {
        id
        ru
        en
        img
        description
        colors {
          id
          code
        }
        sizes {
          id
          size
          description
        }
      }
    }
  }
`