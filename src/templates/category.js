import React from 'react'
import { graphql } from "gatsby"
import SEO from "../components/seo"
import Layout from "../components/layout"
import { injectIntl, Link, FormattedMessage, useIntl } from "gatsby-plugin-intl"
import Card from "../components/main/card"

const CategoryTemplate = ({data}) => {
  const intl = useIntl()
  return (
    <Layout>
      <SEO title={intl.locale === "en" ? data.Api.category.en : data.Api.category.ru} keywords="123" description="123"/>
      <section className="page__section section__bg">
        <div className="container align-items-center">
          <h1 className="page__section--heading">
            {intl.locale === "en" ? data.Api.category.en : data.Api.category.ru}
          </h1>
        </div>
      </section>
      <section className="page__section">
        <div className="container">
          <div className="row">
            <div className="col-12 d-flex flex-wrap justify-content-center">
              {data.Api.category.goods.length !== 0
                ? data.Api.category.goods.map((good) => {
                  return (
                    <Card key={good.id} title={intl.locale === "en" ? good.en : good.ru} link={`/goods/${good.en.toLowerCase()}`} img={good.img}/>
                  )
                })
                : <h2 className="text-center mx-auto">
                  <FormattedMessage id={"goods.notfound"} />
                </h2>
              }
            </div>
          </div>
        </div>
      </section>
    </Layout>
  )
}

export default CategoryTemplate

export const query = graphql`
  query Category ($id: ID!) {
    Api {
      category (id: $id) {
        id
        ru
        en
        goods {
          id
          ru
          en
          img
        }
      }
    }
  }
`