import React from 'react';
import { Provider } from 'react-redux';
import { PersistGate } from 'redux-persist/integration/react';
import { persistReducer, persistStore } from 'redux-persist';
import { createStore } from 'redux';
import storage from 'redux-persist/lib/storage';
import { composeWithDevTools } from "redux-devtools-extension"

export default ({ element }) => {

  const persistConfig = {
    key: 'root',
    storage,
    blacklist: [
      // 'count',
    ],
  };

  const initialState = {
    cart: []
  }

  const rootReducer = (state = initialState, action) => {
    switch (action.type) {
      case "ADDTOCART":
        return Object.assign({}, state, {
          cart: [
            ...state.cart,
            action.payload
          ]
        })
      case "REMOVEGOOD":
        let cart = state.cart
        let index = cart.findIndex(n => n.id === action.payload.id);
        cart.splice(index, 1)
        return Object.assign({}, state, {
          cart: [
            ...cart
          ]
        })
      case "RESETCART":
        return Object.assign({}, state, {
          cart: []
        })
      default:
        return state
    }
  }

  const persistedReducer = persistReducer(persistConfig, rootReducer);
  const store = createStore(persistedReducer, composeWithDevTools());
  const persistor = persistStore(store);

  return (
    <Provider store={store}>
      <PersistGate loading={null} persistor={persistor}>
        {element}
      </PersistGate>
    </Provider>);
}